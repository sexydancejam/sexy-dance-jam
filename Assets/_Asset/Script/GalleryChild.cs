﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GalleryChild : MonoBehaviour {


	public int GalleryIndex;
	public Image GalleryImage;
	public Text txtTanya;

	public void SetImage(int index){
		GalleryIndex = index;

		if(GameMaster.Instance.GalleryStatus[GalleryIndex]){			
			GalleryImage.sprite = GameMaster.Instance.GallerySprite[index];
			txtTanya.gameObject.SetActive(false);
		} else {
			GalleryImage.sprite = GameMaster.Instance.NullGalerrySprite;
			txtTanya.gameObject.SetActive(true);

		}

	 

	}

}
