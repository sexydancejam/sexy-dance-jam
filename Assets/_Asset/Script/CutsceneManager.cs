﻿using UnityEngine;
using System.Collections;
using Thinksquirrel.Utilities;

#region Documentation
	//This script is used for Cutscene Manager, 
	//this script will manage which sprite  will be used 
	//for cutscene and force their transition to be finished instantly
#endregion

public class CutsceneManager : MonoBehaviour {
	
#region Defining Variable

	public UITweener[] CutsceneQueue;
	public UITweener[] CutsceneSprite;

	UITweener LastSprite;

	int currentQueue = 0;
	bool EndCutscene = false;

	public UILabel SignLabel;

	public string NextLevelName;

#endregion

#region Basic Function/Method

	//Remove UI Event Delegetas when this script disabled
	void OnDisable()
	{
		EventDelegate.Remove(LastSprite.onFinished, OnLastSpriteFinished);
	}

	void Awake()
	{
		//Get the last sprite from the last object on CutsceneSprite variable
		GetAllTweenChild();
	}

	void Start()
	{
		AudioController.PlayMusic("Opening"); 
		SetSignLabelText("Skip");
	}

	void Update()
	{
		//Check if the player touch the device screen or mouse button clicked, 
		//then skip the cutscene
		if(Input.GetMouseButtonDown(0))
		{
			if(!EndCutscene)
			{
//				Util.TestDebug("Skip Cutscene");
				SkipCutscene();
			}
			else if( EndCutscene )
			{
				//If cutscene queue length is 1, then load next level,
				//but if the cutscene had more than 1 queue, 
				//go to next cutscene queue
				if(currentQueue < CutsceneQueue.Length-1)
					NextCutsceneQueue();
				else	
					Util.LoadNextLevel(NextLevelName);
			}
		}
	}


#endregion

#region New Method/Function

	//This method will be called when the last sprite finished their tween
	void OnLastSpriteFinished()
	{
//		Util.TestDebug("Last Sprite Finished");
		SkipCutscene();
	}

	//Get all uitweener from Cutscene Group
	void GetAllTweenChild()
	{
		CutsceneSprite = CutsceneQueue[currentQueue].transform.GetComponentsInChildren<UITweener>();
		LastSprite = CutsceneSprite[CutsceneSprite.Length-1];

		EventDelegate.Add(LastSprite.onFinished, OnLastSpriteFinished);
	}

	//Function to Skip Cutscene
	void SkipCutscene()
	{
		SetSignLabelText("Next");

		EndCutscene = true;
		
		for(int i=0; i<CutsceneSprite.Length; i++)
		{
			CutsceneSprite[i].duration = 0;
			CutsceneSprite[i].delay = 0;
			
			CutsceneSprite[i].ResetToBeginning();
			
			CutsceneSprite[i].PlayForward();
		}
	}
	
	//Function to go to next cutscene queue
	void NextCutsceneQueue()
	{
		SetSignLabelText("Skip");

		if(CutsceneQueue.Length>1)
		{
			CutsceneQueue[currentQueue].PlayReverse();

			currentQueue++;

			GetAllTweenChild();

			CutsceneQueue[currentQueue].PlayForward();

			EndCutscene = false;
		}
	}

	//function to set sign label on the bottom right
	void SetSignLabelText(string text)
	{
		SignLabel.text = text;
	}

#endregion
}
