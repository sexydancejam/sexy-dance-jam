﻿using UnityEngine;
using System.Collections;

#region Documentation
	//This script is used for Utilty Script for the game, 
	//All the method in this class can/should be called from other class via static type 
#endregion

public class Util : MonoBehaviour {

#if UNITY_EDITOR

//	public static void TestDebug(string DebugValue)
//	{
//		Debug.Log(DebugValue);
//	}

#endif
	
	public static void LoadNextLevel(string LevelToLoaded)
	{
		Application.LoadLevel(LevelToLoaded);
	}
}
