﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


[ExecuteInEditMode]

public class DFader : MonoBehaviour {

	public bool isLive , isWindowShowed;
	public List<Image> mSprites;
	public List<Text> mText;
	public Color ShowColor , HideColor;
	public float AnimTime;
	public iTween.EaseType mEase;
	

	void Update () {
		if(isLive){
			
			if(isWindowShowed){
				OnUpdateColor(ShowColor);
			} else {
				OnUpdateColor(HideColor);
			}
		}
	}

	public  void HideWindow(){
		if(isWindowShowed){
			
			iTween.ValueTo(gameObject , iTween.Hash(
				"from",ShowColor,
				"to", HideColor,
				"time",0f,
				"easetype",mEase,
				"onupdate","OnUpdateColor",
				"oncomplete" , "OnComplete"
				));	
		} 
	}
	
 

	void OnComplete(){ 
		isWindowShowed = false;
		gameObject.SetActive(false);
	}

	
	public  void ShowWindow(){	 
		iTween.ValueTo(gameObject , iTween.Hash(
			"from",HideColor,
			"to", ShowColor,
			"time",AnimTime,
			"easetype",mEase,
			"onupdate","OnUpdateColor"			
			));		
		isWindowShowed = true;
	}
	
	
	void OnUpdateColor(Color newColor){ 
		
		if( mSprites.Count > 0 ){
			
			for (int i = 0; i < mSprites.Count; i++) {
				mSprites[i].color = newColor;
			} 
		}

		if( mText.Count > 0 ){
			
			for (int i = 0; i < mText.Count; i++) {
				mText[i].color = newColor;
			} 
		}

	}
	
	
	
}
