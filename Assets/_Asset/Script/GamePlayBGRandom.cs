﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GamePlayBGRandom : MonoBehaviour {
	
	
	public List<Material> BGList; 
	public int lastRandom;

	// Use this for initialization
	void Start () {	
		RandomMates();
	}
	
	// Update is called once per frame
	void Update () {

		if(Input.GetKeyDown(KeyCode.Space)){
			RandomMates();
		}
	}

	void RandomMates(){
		
//		renderer.material.SetTexture( "_Diffuse",BGList[RandomUnit(BGList.Count)] );

		renderer.material =  BGList[RandomUnit(BGList.Count)] ;
	}
	
	int result;
	int RandomUnit(int range){
		lastRandom = result;
		result = Random.Range(0,range);


		 if(result == lastRandom){
			return RandomUnit(range);
		} else {
			//lastRandom = result;
						Debug.LogWarning("RandomUnit "+result);
			return result;
		}

	}
	
}
