﻿using UnityEngine;
using System.Collections;



public class InputManager : MonoBehaviour {
	
	public Gameplay game; 
	
	Vector2 FirstFingerPost, LastFingerPost , DeltaMove;
	
	
	
	
	void Update () {
		
		if(Input.GetKeyDown(KeyCode.F) && GameMaster.Instance.isCheat ){ 
			CheckInput(game.NextNote);
		}	
		
		
		switch (GameMaster.Instance.inputControl) {
			
		case InputControl.KEYBOARD:
			Keyboard();
			break;
			
		case InputControl.MOBILE:
			
#if UNITY_EDITOR
			MouseDragInput();
#elif UNITY_ANDROID
			TouchFingerInput();
#endif
			break;
			
		default:
			break;
		}
		
		
		
		
	}	
	
	#region Inputs
	void Keyboard(){ 
		
		if(Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow) ){
			CheckInput(InputType.Swipe_Left);
		}
		
		if(Input.GetKeyDown(KeyCode.D)  || Input.GetKeyDown(KeyCode.RightArrow) ){
			CheckInput(InputType.Swipe_Right);
		}
		
		if(Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow) ){
			CheckInput(InputType.Swipe_Down);
		}
		
		if(Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow) ){
			CheckInput(InputType.Swipe_Up);
		}
		
		
		
	}
	
	void CheckInput(InputType input){
		if(game){
			Debug.Log("Input to gameplay "+input);
			game.CheckInput(input);
		}
	}
	
	void MouseDragInput(){
		
		if(Input.GetKeyDown(KeyCode.Mouse0)){
			Debug.Log("Mouse Down");
			OnStartDrag(Input.mousePosition);
		} 
		
		if(Input.GetKeyUp(KeyCode.Mouse0)){
			Debug.Log("Mouse Up");
			OnDragEnd(Input.mousePosition);
		}
	}
	
	void TouchFingerInput(){
		
		if (Input.touchCount > 0){
			
			Touch touch = Input.touches[0];
			
			switch(touch.phase){
				
			case TouchPhase.Began: 
				OnStartDrag(touch.position);
				break;
				
			case TouchPhase.Ended:  
				OnDragEnd(touch.position);				
				break;
				
			default :
				break;
				
			}
			
		}
	}
	
#endregion
	
	
	void OnStartDrag(Vector2 post){
		FirstFingerPost = post;
	}
	
	
	void OnDragEnd(Vector2 post){
		
		LastFingerPost =post;
		DeltaMove = LastFingerPost-FirstFingerPost ; 		
		if(Mathf.Abs(DeltaMove.x)>= Screen.width*0.1F || Mathf.Abs(DeltaMove.y)>= Screen.height*0.05F ){
			CheckInput(CheckSwipeDirection(DeltaMove));  
		}
	}
	
	
	InputType CheckSwipeDirection(Vector2 dir){ 
		
		if(Mathf.Abs(dir.x) >Mathf.Abs(dir.y) ){ 
			if(dir.x >0){ 
				return InputType.Swipe_Right;
			} else { 
				return InputType.Swipe_Left; 
			}
			
		} else { 
			if(dir.y >0){ 
				return InputType.Swipe_Up; 
			} else { 
				return InputType.Swipe_Down; 
			}
		}
	}
	
	
	
	
}
