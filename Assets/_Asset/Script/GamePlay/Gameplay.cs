﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Gameplay : MonoBehaviour {	
	
	public bool isGameOn ,  isTutorial;
	public int CurrentHealth , CurrentStep , CurrentCombo , MaxCombo , lastRandomInstructor , lastRandomHitInfo;
	

	public InputType NextNote;  
	public Image mInstructor;
	public Slider mTimer; 
	public Text txtCombo;
	public Life mLife;
	//	public DFader GameOverFader;
	public Text txtTotalStep , txtMaxCombo , txtScore , txtTimer;
	
	
	
	void OnEnable(){ 
		EventManager.OnSetNextMoveE+= HandleOnSetNextMoveE;
		EventManager.OnTutorialEndingE += OnTutorialEnding;
	}
	
	
	void OnDisable(){
		EventManager.OnSetNextMoveE-= HandleOnSetNextMoveE; 
		EventManager.OnTutorialEndingE -= OnTutorialEnding;
	}
	
	void OnTutorialEnding (){
		
		StartCoroutine(WaitTutorial());
	} 
	
	IEnumerator WaitTutorial(){
		isGameOn = false;
		txtTimer.gameObject.SetActive(true);
		
		txtTimer.text = "3";
		yield return new WaitForSeconds(1f);
		txtTimer.text = "2";
		yield return new WaitForSeconds(1f);
		txtTimer.text = "1";
		yield return new WaitForSeconds(1f);
		txtTimer.gameObject.SetActive(false);
		isGameOn = true;
		EventManager.TutorialEnd();

		GameMaster.Instance.EndTutorial();
		
		isTutorial = false;
		GameMaster.Instance.TutorialStatus = false;		
		CurrentStep = 0;
		NextNote = RandomNote();
	}
	
	
	void HandleOnSetNextMoveE (InputType input){
		NextNote = input;
		UpdateInstructorImage(NextNote);
	}
	
	
	
	void Start(){
		InitGame();
	}
	
	public void InitGame(){
		txtTimer.gameObject.SetActive(false);
		mLife.gameObject.SetActive(false);
		//		GameOverFader.HideWindow();
		CurrentStep = 0;
		CurrentCombo = 0;
		MaxCombo = 0; 
		//		isCanDetectGesture = true;
		AudioController.PlayMusic("AeroTime"); 
		isGameOn = true;
		CurrentHealth = GameMaster.PlAYER_MAX_LIFE;
		
		isTutorial = GameMaster.Instance.TutorialStatus;
		
		
		if(!isTutorial){
			NextNote = RandomNote();
		} else {
			TutorialManager.Instance.RunTutorial();
		}
		
		
		
		SetTimer(GameMaster.GAME_TIME_NOOB);
		UpdateScore();
	}
	
	public void QuitGame(){
		Debug.Log("QuitGame");
		Application.LoadLevel(SceneNames.scene_menu.ToString());
	}
	
	
	
	
	public void CheckInput(InputType input){	
		//Debug.Log("CheckInput "+input+" NextNote "+NextNote);		
		if(input == NextNote){ 
			OnCorrectInput();
		} else {
			OnWrongInput();
		}	
		
		if(!isTutorial){
			NextNote = RandomNote();
		}		
		SetTimeDifficulty();		 		
		UpdateScore();		
	}
	
	void OnCorrectInput(){	
		EventManager.OnRecieveInput(NextNote);
		
		if(!isTutorial){			
			Debug.Log("Input Benar"); 
			
			CurrentStep++;
			CurrentCombo++;
			if(CurrentCombo > MaxCombo){
				MaxCombo = CurrentCombo;
			}
			
			UpdateHit(true);
		} 
		
		
	}
	
	void OnWrongInput(){
		UpdateHit(false);
		Debug.Log("Input Salah");  
		if(CurrentCombo > MaxCombo){
			MaxCombo = CurrentCombo;
		}
		
		CurrentCombo = 0;
		if(!isTutorial){
			CurrentHealth--;
			mLife.gameObject.SetActive(true);
		}
		if(CurrentHealth <=0){
			Debug.LogWarning("Game Over");  
			OnGameOver();
		}		
	}
	
	public List<string> InfoCorrect , InfoWrong;	
	public Text txtHitInfo;
	
	void UpdateHit(bool value){
		
		if(value){		 
			txtHitInfo.text = InfoCorrect[RandomInfo(InfoCorrect.Count)];
			txtHitInfo.color = Color.green;
		} else {
			txtHitInfo.text = InfoWrong[RandomInfo(InfoWrong.Count)];
			txtHitInfo.color = Color.red;
		}
		iTween.ValueTo(gameObject , iTween.Hash(
			"from",SmallScale,
			"to", BigScale,
			"time",0.2f,
			"easetype",iTween.EaseType.linear,
			"onupdate","OnHitInfoUpdate",
			"oncomplete","OnHitInfoComplete"
			));		
		
	}
	
	
	
	void OnHitInfoUpdate(Vector3 scale){
		txtHitInfo.transform.localScale = scale;
	}
	
	void OnHitInfoComplete(){
		iTween.ValueTo(gameObject , iTween.Hash(
			"from",BigScale ,
			"to", SmallScale,
			"time",0.2f,
			"easetype",iTween.EaseType.linear,
			"onupdate","OnHitInfoUpdate"
			));	
	}
	
	void SetTimeDifficulty(){
		float TimeDiffficulty = GameMaster.GAME_TIME_NOOB;
		
		if(CurrentStep > GameMaster.GAME_STEP_EASY){
			TimeDiffficulty = GameMaster.GAME_TIME_EASY;
		}
		
		if(CurrentStep > GameMaster.GAME_STEP_MEDIUM){
			TimeDiffficulty = GameMaster.GAME_TIME_MEDIUM;
		}
		
		if(CurrentStep > GameMaster.GAME_STEP_HARD){
			TimeDiffficulty = GameMaster.GAME_TIME_HARD;
		}
		
		if(CurrentStep > GameMaster.GAME_STEP_CHAOS){
			TimeDiffficulty = GameMaster.GAME_TIME_CHAOS;
		}
		
		SetTimer(TimeDiffficulty);
	}	
	
	public Vector3 SmallScale, BigScale;
	
	void UpdateScore(){
		//		txtStep.text 	= "Step : "+CurrentStep;
		txtCombo.text 	= ""+CurrentStep;
		
		iTween.ValueTo(gameObject , iTween.Hash(
			"from",SmallScale,
			"to", BigScale,
			"time",0.2f,
			"easetype",iTween.EaseType.linear,
			"onupdate","OnUpdateCombo",
			"oncomplete","OnCompleteCombo"
			));	
		
		//		txtLife.text 	= "Life : "+CurrentHealth;
		mLife.SetLife(CurrentHealth);
	}	
	
	void OnUpdateCombo(Vector3 scale){
		txtCombo.transform.localScale = scale;
	}
	
	void OnCompleteCombo(){
		iTween.ValueTo(gameObject , iTween.Hash(
			"from",BigScale,
			"to", SmallScale,
			"time",0.2f,
			"easetype",iTween.EaseType.linear,
			"onupdate","OnUpdateCombo"			
			));	
	}
	
	public void SetTimer(float GameTime){
		if(!isTutorial){
			
			//		Debug.Log("GameTime "+GameTime);
			mTimer.value = 0;		
			iTween.Stop(gameObject); 
			iTween.ValueTo(gameObject , iTween.Hash(
				"name","TimerTween",
				"from",0f,
				"to", 1f,
				"time",GameTime,
				"easetype",iTween.EaseType.linear,
				"onupdate","OnUpdateBar",
				"oncomplete" , "OnTimesUp"
				));	
		}
		
	}
	
	void OnUpdateBar(float val){
		mTimer.value = val;
		
		if(!isGameOn){
			iTween.Stop("TimerTween");
			mTimer.value = 1f;
		}
	}
	
	void OnTimesUp(){ 
		Debug.Log("Times Up");
		CheckInput(InputType.None); 		
	}
	
	
	
	public void OnGameOver( ){
		isGameOn = false; 		
		GameMaster.Instance.LastGameStep = CurrentStep;
		GameMaster.Instance.LastGameCombo = MaxCombo;
		Application.LoadLevel(SceneNames.scene_ending.ToString());
	}
	
	
	
	InputType RandomNote(){ 
		AudioController.Play("InstructorGrunt");
		InputType result;
		int random;
		
		if(CurrentStep <= GameMaster.GAME_STEP_EASY){
			random = RandomInstructor(2);
		} else  {
			random = RandomInstructor(4);
		}	  
		
		switch (random) {
			
		case 0 :
			result = InputType.Swipe_Right; 	
			break;
		case 1 :
			result = InputType.Swipe_Left;
			
			break;
		case 2 :
			result = InputType.Swipe_Down; 
			break;
		case 3 :
			result = InputType.Swipe_Up; 
			break;
			
		default:
			Debug.Log("Default");
			result = InputType.None;
			break;
		}
		
		UpdateInstructorImage(result);
		return result;
		
	}
	
	
	int RandomInstructor(int range){
		int result = Random.Range(0,range); 
		if(result == lastRandomInstructor){
			return RandomInstructor(range);
		} else {
			lastRandomInstructor = result;			
			return result;
		}
		
	}
	
	int RandomInfo(int range){
		int result = Random.Range(0,range);				
		if(result == lastRandomHitInfo){
			return RandomInfo(range);
		} else {
			lastRandomHitInfo = result;			
			return result;
		}
		
	}
	
	
	void UpdateInstructorImage(InputType input){
		
		switch (input) {
		case InputType.Swipe_Left:
			mInstructor.sprite = GameMaster.Instance.Swipe_Left.GetImage(CurrentStep);
			break;
			
		case InputType.Swipe_Right:
			mInstructor.sprite = GameMaster.Instance.Swipe_Right.GetImage(CurrentStep);
			break;
			
		case InputType.Swipe_Up:
			mInstructor.sprite = GameMaster.Instance.Swipe_Up.GetImage(CurrentStep);
			break;
			
		case InputType.Swipe_Down:
			mInstructor.sprite = GameMaster.Instance.Swipe_Down.GetImage(CurrentStep);
			break;
			
		default:
			break;
		}
	}
	
	
	
	
}
