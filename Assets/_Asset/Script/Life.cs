﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Life : MonoBehaviour {
	
	public Image Mark;
	public Sprite Mark1, Mark2, Mark3;
	
	
	public void SetLife(int life){
		switch (life) {
			
		case 2 :
			Mark.sprite = Mark1;
			break;
			
		case 1 :
			Mark.sprite = Mark2;
			break;
			
		case 0 :
			Mark.sprite = Mark3;
			break;
			
			
		default:
			Mark.sprite = Mark1;
			break;
		}
	}
	
}
