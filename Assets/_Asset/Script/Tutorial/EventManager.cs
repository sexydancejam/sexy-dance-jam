﻿using UnityEngine;
using System.Collections;

public class EventManager : MonoBehaviour {

	public delegate void InputHandler(InputType input);
	
	public static event InputHandler OnRecieveInputE;
	public static void OnRecieveInput(InputType input) {
		if (OnRecieveInputE != null) 
			OnRecieveInputE(input); 
	}

	public static event InputHandler OnSetNextMoveE;
	public static void SetNextMove(InputType input) {
		if (OnSetNextMoveE != null) 
			OnSetNextMoveE(input); 
	}



	public delegate void NormalEvent( );	
	public static event NormalEvent OnTutorialEndE;
	public static void TutorialEnd( ) {
		if (OnTutorialEndE != null) 
			OnTutorialEndE( ); 
	}

	public static event NormalEvent OnTutorialEndingE;
	public static void TutorialEnding( ) {
		if (OnTutorialEndingE != null) 
			OnTutorialEndingE( ); 
	}



	public delegate void TutorialDialog(string dialog );	
	public static event TutorialDialog OnTutorialDialogE;
	public static void DoTutorialDialog(string dialog ) {
		if (OnTutorialDialogE != null) 
			OnTutorialDialogE(dialog ); 
	}


}
