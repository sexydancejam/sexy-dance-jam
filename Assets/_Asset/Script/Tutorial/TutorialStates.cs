using UnityEngine;
using System.Collections;

[System.Serializable]
public class TutState_01: FSMState {
	public TutorialManager parent;

	public TutState_01(TutorialManager parent){
		this.parent = parent;
	}

	public override void OnEnter(){ 
		EventManager.SetNextMove(InputType.Swipe_Left);		
		EventManager.OnRecieveInputE += HandleOnRecieveInputE;
		EventManager.DoTutorialDialog("Pay Attention! Watch my HAND! Now.. move to the LEFT \n[Press LEFT ARROW KEY]");
	}

	public override void OnExit(){
		EventManager.OnRecieveInputE -= HandleOnRecieveInputE;
	}

	void HandleOnRecieveInputE (InputType input){
		if(input == InputType.Swipe_Left){	
			EventManager.SetNextMove(InputType.Swipe_Right);
			EventManager.DoTutorialDialog("Good! Now Move to RIGHT! \n[Press RIGHT ARROW KEY]");
			parent.GoToState(parent.tut_02);
		}
	} 
}

/*******************************************/

[System.Serializable]
public class TutState_02: FSMState {
	public TutorialManager parent;
	
	public TutState_02(TutorialManager parent){
		this.parent = parent;
	}
	
	public override void OnEnter(){
		EventManager.OnRecieveInputE += HandleOnRecieveInputE;
	}
	
	public override void OnExit(){
		EventManager.OnRecieveInputE -= HandleOnRecieveInputE;
	}
	
	void HandleOnRecieveInputE (InputType input){
		
		if(input == InputType.Swipe_Right){		 
			EventManager.SetNextMove(InputType.Swipe_Up);	
			EventManager.DoTutorialDialog("Very Good! Lets Try Squat, Down and... UP! \n[Press UP ARROW KEY]");
			parent.GoToState(parent.tut_03);
		}
	} 
}

/*******************************************/

[System.Serializable]
public class TutState_03: FSMState {
	public TutorialManager parent;
	
	public TutState_03(TutorialManager parent){
		this.parent = parent;
	}
	
	public override void OnEnter(){
		EventManager.OnRecieveInputE += HandleOnRecieveInputE;
	}
	
	public override void OnExit(){
		EventManager.OnRecieveInputE -= HandleOnRecieveInputE;
	}
	
	void HandleOnRecieveInputE (InputType input){
		
		if(input == InputType.Swipe_Up){
			EventManager.SetNextMove(InputType.Swipe_Down);
			EventManager.DoTutorialDialog("Still Trying Squat, Up and... DOWN! \n[Press DOWN ARROW KEY]");
			parent.GoToState(parent.tut_04);
		}
	} 
}

/*******************************************/
[System.Serializable]
public class TutState_04: FSMState {
	public TutorialManager parent;
	
	public TutState_04(TutorialManager parent){
		this.parent = parent;
	}
	
	public override void OnEnter(){
		EventManager.SetNextMove(InputType.Swipe_Down);
		EventManager.OnRecieveInputE += HandleOnRecieveInputE;
	}
	
	public override void OnExit(){
		EventManager.OnRecieveInputE -= HandleOnRecieveInputE;
	}
	
	void HandleOnRecieveInputE (InputType input){
		
		if(input == InputType.Swipe_Down){
			parent.GoToState(parent.tut_05);
			EventManager.DoTutorialDialog("Heyy, Looking good, try to keep it up, maybe I'll give you some... REWARD.. hihihi...\n[Press ANY ARROW KEY]");
		}
	} 
}

/*******************************************/

[System.Serializable]
public class TutState_05: FSMState {
	public TutorialManager parent;
	
	public TutState_05(TutorialManager parent){
		this.parent = parent;
	}
	
	public override void OnEnter(){		

		EventManager.TutorialEnding();

	}
 
}
