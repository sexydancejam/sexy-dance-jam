using UnityEngine;
using System.Collections;
/*
 * Manages steps in the tutorials.
 * Tutorial Database : 
 * https://docs.google.com/a/touchten.com/spreadsheets/d/1G5pRqIWUg_VvwiTWOfx6NI2D3EufXdgpNaqn7OTMPuY/edit#gid=0
 */

public class TutorialManager : FSMSystem
{
 
	public TutState_01 tut_01;
	public TutState_02 tut_02;
	public TutState_03 tut_03;
	public TutState_04 tut_04;
	public TutState_05 tut_05;

	public static TutorialManager Instance;

 

	void Awake(){
		Instance = this;
	}

	void Start(){
		tut_01 = new TutState_01(this);
		tut_02 = new TutState_02(this);
		tut_03 = new TutState_03(this);
		tut_04 = new TutState_04(this);
		tut_05 = new TutState_05(this);

		AddState(tut_01);  
		AddState(tut_02); 
		AddState(tut_03); 
		AddState(tut_04); 
		AddState(tut_05);	

	 
	}

	public void RunTutorial(){
		GoToState(tut_01);
	}


	 
	
}