﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TutorialDialog : MonoBehaviour {

	public Text txtDialog;
	public Image Bg;

	void OnEnable(){
		EventManager.OnTutorialDialogE += SetDialog ;
		EventManager.OnTutorialEndE += HideDialog;
	}

	void OnDisable(){
		EventManager.OnTutorialDialogE -= SetDialog ;
		EventManager.OnTutorialEndE -= HideDialog;
	}
 
	
	public void HideDialog(){
		Bg.gameObject.SetActive(false);
	}


	public void SetDialog(string dialog){
		Bg.gameObject.SetActive(true);
		txtDialog.text = dialog;
	}
}
