﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]

public class DScroller : MonoBehaviour {
	
	public bool isLive , isCanBeDeactivated , isWindowShowed;
	public RectTransform mWindow;
	public Vector2 ShowPost , HidePost;
	public float AnimTime;
	public iTween.EaseType mEase;
	
	
	void Update () {
		if(isLive){
			
			if(isWindowShowed){
				OnUpdatePost(ShowPost);
			} else {
				OnUpdatePost(HidePost);
			}
		}
	}
	
	
	public  void HideWindow(){

		if(isWindowShowed){
			iTween.ValueTo(gameObject , iTween.Hash(
				"from",ShowPost,
				"to", HidePost,
				"time",AnimTime,
				"easetype",mEase,
				"onupdate","OnUpdatePost"			
				));	
		}

		isWindowShowed = false;
	}

	public  void HideAndDisable(){
		if(isWindowShowed){
			iTween.ValueTo(gameObject , iTween.Hash(
				"from",ShowPost,
				"to", HidePost,
				"time",AnimTime,
				"easetype",mEase,
				"onupdate","OnUpdatePost",
				"oncomplete", "OnComplete"
				));	
		}
		
		isWindowShowed = false;
	}

	void OnComplete(){
		if(isCanBeDeactivated){
			gameObject.SetActive(false);
		}

	}
	
	public  void QuickHide(){
		if(isWindowShowed){
			OnUpdatePost(HidePost);	 
		}
	
		isWindowShowed = false;
	}
	
	public  void ShowWindow(){		
		iTween.ValueTo(gameObject , iTween.Hash(
			"from",HidePost,
			"to", ShowPost,
			"time",AnimTime,
			"easetype",mEase,
			"onupdate","OnUpdatePost"			
			));		
		isWindowShowed = true;
	}
	
	
	void OnUpdatePost(Vector2 newPos){ 
		mWindow.anchoredPosition = newPos;
	}
	
	
	
}
