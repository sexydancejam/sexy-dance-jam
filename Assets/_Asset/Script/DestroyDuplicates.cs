using UnityEngine;
using System.Collections;

public class DestroyDuplicates : MonoBehaviour {
	
	void OnLevelWasLoaded(int level){
		//Debug.Log ("Singleton");
		GameObject[] objectsInScene = FindObjectsOfType(typeof(GameObject)) as GameObject[];
		
		foreach(GameObject objectInScene in objectsInScene)
		{			
			if ((gameObject.name == objectInScene.name + "(Clone)" || gameObject.name == objectInScene.name) && gameObject != objectInScene)
			{
				Debug.LogWarning ("A duplicate of object " + gameObject.name + " found. Destroying duplicates.", gameObject); 
				DestroyImmediate(objectInScene);
				return;
			}
		}
	}
}
