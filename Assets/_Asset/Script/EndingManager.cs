﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

#region Documentation
//This script is used for Ending Manager, 
//this script will manage which Ending will be shown
//when the games over
#endregion

public class EndingManager : MonoBehaviour {

#region Defining Variable

	public UITweener[] EndingGroup;
	UITweener[] currentEndingSprite;

	public UISprite BGTexture;
	public UILabel ContinueLabel;

	bool isShowResult;
	public int EndingToPlay = 1;

	public Text txtStep, txtCombo , txtScore;
	public GameObject ScoreBoard;
#endregion


#region Basic Function/Method

	void Awake()
	{
		SetText("");
		isShowResult = false;
	}

	void Start()
	{
		Debug.Log("Play Music GameOver");
		AudioController.PlayMusic("GameOver"); 
		ScoreBoard.gameObject.SetActive(false);
		DecideEnding();
	}


	void DecideEnding(){

		txtStep.text = GameMaster.Instance.LastGameStep +"" ;
		txtCombo.text =  GameMaster.Instance.LastGameCombo +"" ;
		txtScore.text = (GameMaster.Instance.LastGameStep +  GameMaster.Instance.LastGameCombo)+"";

		int TargetEnding = 0;

		if(GameMaster.Instance.LastGameStep > GameMaster.UNLOCK_IMAGE_1 ){
			TargetEnding = 1;
		}


		if(GameMaster.Instance.LastGameStep > GameMaster.UNLOCK_IMAGE_2 ){
			TargetEnding = 2;
		}

		if(GameMaster.Instance.LastGameStep > GameMaster.UNLOCK_IMAGE_3 ){
			TargetEnding = 3;
		}

		if(GameMaster.Instance.LastGameStep > GameMaster.UNLOCK_IMAGE_4 ){
			TargetEnding = 4;
		}

 
		GameMaster.Instance.UnlockGallery(TargetEnding);
		PlayEnding(TargetEnding); 
	}

	void Update()
	{
		if(Input.GetMouseButtonDown(0))
		{
			if(isShowResult)
			{
				WaitShowScore();
			}
		}
	}
#endregion

#region New Method

	//Function to play ending
	//which ending to be played, refer to gameplay
	void PlayEnding(int EndingQueue)
	{
		BGTexture.spriteName = "BG_" + EndingQueue;
		EndingGroup[EndingQueue].PlayForward();

		currentEndingSprite = EndingGroup[EndingQueue].GetComponentsInChildren<UITweener>();
	}

	public void CallResultScreen()
	{
		SetText("Next");

		isShowResult = true;
	}

	void WaitShowScore(){
		if(isShowResult)
		{
			isShowResult = false;

			SetText("");

			ScoreBoard.gameObject.SetActive(true);
			SetScores();
		}

	}

	void SetText(string text)
	{
		ContinueLabel.text = text;
	}
	
	public void OnReplay(){
		Application.LoadLevel(SceneNames.scene_game.ToString());
	}

	public void OnQuit(){
		Application.LoadLevel(SceneNames.scene_menu.ToString());
	}

	public void SetScores(){
		txtStep.text = GameMaster.Instance.LastGameStep +"" ;
		txtCombo.text =  GameMaster.Instance.LastGameCombo +"" ;
		txtScore.text = (GameMaster.Instance.LastGameStep +  GameMaster.Instance.LastGameCombo)+"";
	}

#endregion
}
