﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public enum UIType{
	MainMenu, Gallery , Credits
}


public class MenuController : MonoBehaviour {
	
	
	public UIType uiType;
	public GameObject ChildPrefab , mFader;
	public Image ZoomGallery;
	public Transform GalleryParent;
	public DScroller scrollMain , scrollBack , scrollGallery , scrollCredit;
	
	public List<GalleryChild> ChildList;
	
	
	void OnEnable(){
		Gesture.onShortTapE+= HandleonShortTapE;
	}
	
	void OnDisable(){
		Gesture.onShortTapE-= HandleonShortTapE;
	}
	
	void HandleonShortTapE (Vector2 pos){
		
	}
	
	void Start(){
		AudioController.PlayMusic("Opening"); 
		InitGalleryChild();
	}
	
	public void OnButtonPlay(){
		if(GameMaster.Instance.TutorialStatus){
			Application.LoadLevel(SceneNames.scene_opening+"");
		} else{
			Application.LoadLevel(SceneNames.scene_game+"");
		}
		
	} 
	
	public void OnButtonPlayTutorial(){
		GameMaster.Instance.TutorialStatus = true;
		Application.LoadLevel(SceneNames.scene_opening+"");
		
		
	} 
	
	public void OnButtonBack (){
		mFader.SetActive(false);
		uiType = UIType.MainMenu;
		scrollMain.ShowWindow();
		scrollBack.HideWindow();
		scrollGallery.HideWindow();
		scrollCredit.HideWindow();
		
	}
	
	public void OnButtonGallery(){
		mFader.SetActive(true);
		uiType = UIType.Gallery;
		scrollGallery.ShowWindow();
		scrollMain.HideWindow();
		scrollBack.ShowWindow();
		RefreshGallery();
	}
	
	void InitGalleryChild(){
		for (int i = 0; i < GameMaster.GALLERY_SIZE; i++) {
			GameObject newContent = Instantiate(ChildPrefab) as GameObject; 	
			
			newContent.transform.SetParent(GalleryParent.transform);  
			newContent.transform.localScale = Vector3.one;
			newContent.name = "GalleryImage_"+i;
			GalleryChild newChild = newContent.GetComponent<GalleryChild>(); 	 
			newChild.SetImage(i);
			newContent.GetComponent<Button>().onClick.AddListener( ()=> OnGalleryChildTap(newChild.GalleryIndex) );
			
			ChildList.Add(newChild);
			
		}
	}
	
	
	void RefreshGallery(){
		for (int i = 0; i < ChildList.Count; i++) {
			ChildList[i].SetImage(i);
		}
	}
	
	public void OnGalleryChildTap(int galleryIndex){
		
		if(GameMaster.Instance.GalleryStatus[galleryIndex]){
			ZoomGallery.gameObject.SetActive(true);
			ZoomGallery.sprite = GameMaster.Instance.GallerySprite[galleryIndex];
		}	else { 
			ZoomGallery.sprite = GameMaster.Instance.NullGalerrySprite;
		}
		
	}
	
	public void OnZoomGalleryTap(){
		ZoomGallery.gameObject.SetActive(false);
	}
	
	public void OnButtonCredits(){
		mFader.SetActive(true);
		uiType = UIType.Credits;
		scrollCredit.ShowWindow();
		scrollMain.HideWindow();
		scrollBack.ShowWindow();
	}
	
	public void OnQuit(){
		Application.Quit();
	}
	
}
