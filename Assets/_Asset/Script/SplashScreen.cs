﻿using UnityEngine;
using System.Collections;

public class SplashScreen : MonoBehaviour {


	public Animator mAnimator;
	public DFader mFader;

	void Start () {
		StartCoroutine(SplashSequence());

	}

	IEnumerator SplashSequence(){
		mAnimator.SetTrigger("DoSplash");
		yield return new WaitForSeconds(2f); 
		mFader.ShowWindow();
		yield return new WaitForSeconds(2f);
		Application.LoadLevel(SceneNames.scene_menu.ToString());
	}
	 
}
