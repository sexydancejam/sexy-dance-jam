﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


[System.Serializable]
public class DanceType{
	public  InputType input;
	public  List<Sprite> image;
	int lastRandom;
	
	public DanceType(InputType inp , Sprite img ){
		image = new List<Sprite>();
		this.input = inp;
		this.image.Add(img);
	}
	
	public Sprite GetImage(int StepCount){
		
		int randomIndex = 0;
		
		if(StepCount > GameMaster.GAME_STEP_EASY){
			randomIndex = RandomUnit(2);
		}
		
		if(StepCount > GameMaster.GAME_STEP_MEDIUM){
			randomIndex = RandomUnit(3);
		}
		
		if(StepCount > GameMaster.GAME_STEP_HARD){
			randomIndex = RandomUnit(4);
		}
		
		if(StepCount > GameMaster.GAME_STEP_CHAOS){
			randomIndex = RandomUnit(5);
		}
		
		
		if(randomIndex >= image.Count){
			randomIndex = image.Count - 1;
		}
		Sprite result = image[randomIndex];
		return result;
	}
	
	int RandomUnit(int range){
		int result = Random.Range(0,range);
		
		Debug.LogWarning("RandomUnit "+result + " LastRandom "+lastRandom);
		
		
		if(result == lastRandom){
			return RandomUnit(range);
		} else {
			lastRandom = result;
			
			return result;
		}
		
	}
	
	
	
}

public enum InputControl{
	KEYBOARD, MOBILE
}

public enum InputType{
	Swipe_Up , Swipe_Left , Swipe_Down , Swipe_Right  , None
}


public enum SceneNames{
	scene_opening , scene_game , scene_ending , scene_menu
	
}




public class GameMaster : MonoBehaviour {
	
	public static GameMaster Instance;
	public static int PlAYER_MAX_LIFE = 3;
	
	public static float GAME_TIME_NOOB = 5f;
	public static float GAME_TIME_EASY = 3f;
	public static float GAME_TIME_MEDIUM = 2f;
	public static float GAME_TIME_HARD = 1f;
	public static float GAME_TIME_CHAOS = 0.5f;
	
	public static int GAME_STEP_NOOB = 0;
	public static int GAME_STEP_EASY = 10;
	public static int GAME_STEP_MEDIUM = 40;
	public static int GAME_STEP_HARD = 100;
	public static int GAME_STEP_CHAOS = 200;


	public static int UNLOCK_IMAGE_1 = 20;
	public static int UNLOCK_IMAGE_2 = 50;
	public static int UNLOCK_IMAGE_3 = 100;
	public static int UNLOCK_IMAGE_4 = 150; 
	
	public static int GALLERY_SIZE = 5;
	
	public InputControl inputControl;
	
	
	
	public bool TutorialStatus; 
	
	public  DanceType Swipe_Up , Swipe_Left , Swipe_Down , Swipe_Right  ;
	public  Sprite NullGalerrySprite;
	public  List<Sprite> GallerySprite;
	
	public List<bool> GalleryStatus;
	
	public int LastGameStep , LastGameCombo;
	public bool isCheat;

	void Awake(){
		Instance = this;   

		try {
			LoadData();
		} catch (System.Exception ex) {
			
		}
	}
	
	
	public void SaveData(){
		Debug.Log("Saving");
		ES2.Save(GalleryStatus, "gallery_status");
		ES2.Save(TutorialStatus , "tutorial_status");
	}
	
	public void LoadData(){
		Debug.Log("Loading");
		GalleryStatus = ES2.LoadList<bool>("gallery_status");
		TutorialStatus = ES2.Load<bool>("tutorial_status");
	}
	
	
	void Update(){
		
		
		if(Input.GetKeyDown(KeyCode.Alpha1)){
			SaveData();
		}
		
		if(Input.GetKeyDown(KeyCode.Alpha2)){
			LoadData();
		}
		
	}

	public void EndTutorial(){
		TutorialStatus = false;
		ES2.Save(TutorialStatus , "tutorial_status");
	}

	public void UnlockGallery(int index){
		
		if(GalleryStatus[index] == false){
			GalleryStatus[index] = true;
			SaveData();
		}
		
	}
	
	
	
}
