using UnityEngine;
using System.Collections;

public class Essentials : MonoBehaviour {
	
	public GameObject[] objectsToLoad;
	
	void Awake () {
		GameObject[] objectsInScene = FindObjectsOfType(typeof(GameObject)) as GameObject[];
		
		foreach(GameObject obj in objectsToLoad)
		{
			bool objectFound = false;
			foreach(GameObject objectInScene in objectsInScene)
			{
				if (objectInScene.name == obj.name || objectInScene.name == obj.name + "(Clone)")
				{
					objectFound = true;
					break;
				}
			}
			
			if(!objectFound)
			{
				GameObject theObj = Instantiate(obj) as GameObject;
				DontDestroyOnLoad(theObj);
				theObj.transform.parent = this.transform;
				theObj.AddComponent<DestroyDuplicates>();
			}
		}

		this.gameObject.AddComponent<DestroyDuplicates>();
		DontDestroyOnLoad(this.gameObject);
	}
}
