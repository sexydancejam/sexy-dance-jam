﻿using UnityEngine;
using System.Collections;
using Thinksquirrel.Utilities;

#region Documentation
//This script is used for Shake Manager, 
//this script will shake the NGUI Camera Position.

//Note : adjust the shake effect from inspector view
#endregion

public class ShakeEvent : MonoBehaviour {

#region Defining Variable
	Camera CutsceneCam;
	CameraShake CamShake;
#endregion

#region Basic Function/Method
	void Awake()
	{
		CutsceneCam = Camera.main;

		if(CamShake == null)
			CamShake = CutsceneCam.GetComponent<CameraShake>();
	}

	void Start()
	{
		if(CamShake == null)
		{
			CutsceneCam.gameObject.AddComponent<CameraShake>();
			SetShakeEffect(CameraShake.ShakeType.LocalPosition, 10, 5);
		}
	}
#endregion

#region New Function/Method
	void SetShakeEffect(CameraShake.ShakeType shakeType, int NumOfShakes, int ShakeDirectionAmount)
	{
		CamShake = CutsceneCam.GetComponent<CameraShake>();

		CamShake.shakeType = CameraShake.ShakeType.LocalPosition;
		CamShake.numberOfShakes = NumOfShakes;
		CamShake.shakeAmount = new Vector3(ShakeDirectionAmount,ShakeDirectionAmount,ShakeDirectionAmount);
		CamShake.rotationAmount = new Vector3(ShakeDirectionAmount, ShakeDirectionAmount, ShakeDirectionAmount);
	}

	public void PlayCameraShaking()
	{
		CamShake.Shake();
	}
#endregion

}
